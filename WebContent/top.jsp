<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<title>簡易Twitter</title>
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<c:if test="${ empty loginUser }">
        			<a href="login">ログイン</a>
        			<a href="signup">登録する</a>
    			</c:if>
			    <c:if test="${ not empty loginUser }">
			        <a href="./">ホーム</a>
			        <a href="setting">設定</a>
			        <a href="logout">ログアウト</a>
			    </c:if>
			</div>
			<c:if test="${ not empty loginUser }">
			    <div class="profile">
			        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			        <div class="account">@<c:out value="${loginUser.account}" /></div>
			        <div class="description"><c:out value="${loginUser.description}" /></div>
			    </div>
			</c:if>

			<c:if test="${ not empty errorMessages }">
		    <div class="errorMessages">
		        <ul>
		            <c:forEach items="${errorMessages}" var="errorMessage">
		                <li><c:out value="${errorMessage}" />
		            </c:forEach>
		        </ul>
		    </div>
		    <c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="form-area">
			    <c:if test="${ isShowMessageForm }">
			        <form action="message" method="post">
			            いま、どうしてる？<br />
			            <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
			            <br />
			            <input type="submit" value="つぶやく">（140文字まで）
			        </form>
			    </c:if>
			</div>

			<div class="date-filter">
				<form action="./" method="get">
					日付：
					<input type="date" name="start" value="${ start }" <c:out value="${ start }"/>>～
					<input type="date" name="end" value="${ end }" <c:out value="${ end }"/>>
					<input type="submit" value="絞込">
				</form>
			</div>

			<%-- つぶやきの表示 --%>
			<div class="messages">
				<%-- DBのつぶやきデータをforEachで繰り返し表示 --%>
			    <c:forEach items="${messages}" var="message">
			        <div class="message">
			            <div class="account-name">
			                <span class="account">
							    <a href="./?user_id=<c:out value="${message.userId}"/> ">
							        <c:out value="${message.account}" />
							    </a>
							</span>
			                <span class="name"><c:out value="${message.name}" /></span>
			            </div>
			            <pre class="text"><c:out value="${message.text}" /></pre>
			            <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			            <c:if test="${ loginUser.id == message.userId && not empty loginUser}">
				            <div class="message-button">
					            <%-- つぶやきの編集 --%>
					            <form action="edit" method="get" class="message-button">
					            	<input name="id" value="${message.id}" id="id" type="hidden"/>
					            	<button type="submit">編集</button>
					            </form>
					            <%-- つぶやきの削除 --%>
					            <form action="deleteMessage" method="post" class="message-button">
					            	<input name="id" value="${message.id}" id="id" type="hidden"/>
					            	<button type="submit">削除</button>
					            </form>
				            </div>
			            </c:if>

						<%-- 返信の表示 --%>
						<div class="comments">
							<%-- DBの返信データをforEachで繰り返し表示 --%>
							<c:forEach items="${comments}" var="comment">
								<c:if test="${comment.messageId == message.id}">
									<div class="comment">
										<div class="comment^arrow">→</div>
										<div class="comment-container">
											<div class="account-name">
												<span class="account"><c:out value="${comment.account}" /></span>
												<span class="name"><c:out value="${comment.name}" /></span>
											</div>
											<pre class="text"><c:out value="${comment.text}" /></pre>
											<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
										</div>
									</div>
								</c:if>
							</c:forEach>
						</div>
				        <div class="comment-area">
							<c:if test="${ isShowMessageForm }">
								<form action="comment" method="post">
									返信<br />
									<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
									<br />
									<input type="hidden" name="id" id="id" value="${message.id}">
									<input type="submit" value="返信">（140文字まで）
								</form>
							</c:if>
						</div>
			        </div>
			    </c:forEach>
			</div>

			<div class="copyright"> Copyright(c)Kotaro Tsubaki</div>
		</div>
	</body>
</html>