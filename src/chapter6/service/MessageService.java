package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //top.jsp画面につぶやきを表示するためのDB接続メソッド
    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
            //id(ユーザーID)がトップ画面でGETされていたら型変換
            if (!StringUtils.isEmpty(userId)) {
              id = Integer.parseInt(userId);
            }
            //start(最初の日付)がトップ画面でGETされていたらその日付を代入
            //されていなかったら初期値を代入
            if (!StringUtils.isEmpty(start)) {
            	start = start + " 00:00:00";
            } else {
            	start = "2020/01/01 00:00:00";
            }
            //end(最後の日付)がトップ画面でGETされていたらその日付を代入
            //されていなかったら現在時刻を代入
            if (!StringUtils.isEmpty(end)) {
            	end = end + " 00:00:00";
            } else {
            	Date nowDate = new Date();
            	SimpleDateFormat nowDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                end = nowDateFormat.format(nowDate);
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, start, end, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //メッセージIDに紐づく、つぶやきを探すためのDB接続メソッド
    public Message select(int messageId) {
    	Connection connection = null;
        try {
            connection = getConnection();
            Message message = new MessageDao().select(connection, messageId);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきを削除するためのDB接続メソッド
    public void delete(int messageId) {
    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきを編集するためのDB接続メソッド
    public void update(Message message) {
    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}