package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns= {"/setting", "/edit"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;

		//セッションが存在しない場合はnullを返す
		HttpSession session = (httpRequest).getSession(false);

		if (session != null) {
			// 何かしらセッションが存在する
			Object loginCheck = session.getAttribute("loginUser");
			if (loginCheck != null) {
				//loginUserオブジェクトがセッションに存在する場合、通常通りの遷移
				chain.doFilter(request, response);
				return;
			}
		}

		//loginUserオブジェクトがセッションに存在しない場合、エラー文を渡してログイン画面に遷移
		String errorMessages = "ログインしてください";
		session.setAttribute("errorMessages", errorMessages);
		httpResponse.sendRedirect(httpRequest.getContextPath() + "/login");
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
