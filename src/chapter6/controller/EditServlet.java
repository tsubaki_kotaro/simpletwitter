package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		//セッションを取得（ログイン中のUserオブジェクトが保存されている）
		HttpSession session = request.getSession();

		List<String> errorMessages = new ArrayList<String>();
		String messageId = request.getParameter("id");

		Message message = null;
        //GETされたメッセージIDが一文字以上の数字かつnullでない場合のみ処理を実行
        if (!StringUtils.isBlank(messageId) && messageId.matches("^[0-9]+$")) {
        	int numberMessageId = Integer.parseInt(request.getParameter("id"));
        	message = new MessageService().select(numberMessageId);
        }

        //GETされたメッセージIDが一文字以上の数字かつnullの場合エラー
        //エラー文をセッションへ投げてホームにリダイレクト
        if (message == null) {
        	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
        }

        request.setAttribute("message", message);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
    }

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<String> errorMessages = new ArrayList<String>();

        Message message = getMessage(request);
        //エラーの有無を確認し、エラーなしの場合処理を進める
        if (isValid(message, errorMessages)) {
            try {
                new MessageService().update(message);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        //エラーがあった時の処理。エラー文等をJSPに投げる
        if (errorMessages.size() != 0) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }
        request.setAttribute("message", message);
        response.sendRedirect("./");
    }

	//JSPから投げられた値をMessageオブジェクトに格納するメソッド
	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		//Messageオブジェクトを生成しedit.jspからPOSTされたname=textとname=idを格納
        Message message = new Message();
        message.setId(Integer.parseInt(request.getParameter("id")));
        message.setText(request.getParameter("text"));
        return message;
    }

	//つぶやき文のエラー有無を確認するメソッド
	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
